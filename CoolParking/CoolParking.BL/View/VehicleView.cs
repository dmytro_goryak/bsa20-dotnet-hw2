﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text;

namespace CoolParking.BL.View
{
    class VehicleView : IView
    {
        private bool QuitFlag = true;
        ParkingService _parkingServices;
        public VehicleView(ParkingService parkingService)
        {
            _parkingServices = parkingService;
        }
        public void Start()
        {
            Console.Clear();
            while (QuitFlag)
            {
                #region ConsoleWrite
                Console.BackgroundColor = ConsoleColor.Cyan;
                Console.ForegroundColor = ConsoleColor.DarkBlue;

                Console.WriteLine("\nVehicle management in the parking lot.\n");

                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.Cyan;

                Console.WriteLine("\t1. Put the vehicle on the parking");
                Console.WriteLine("\t2. Top up the balance of a specific vehicle");
                Console.WriteLine("\t3. Remove the vehicle from the parking");

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tB. Back to main menu.");

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\nYour option : ");
                #endregion

                switch (Console.ReadLine().ToLower())
                {
                    case "1":

                        try
                        {
                            PutVehicle();
                        }
                        #region catch
                        catch (ArgumentException e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        catch (Exception e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        #endregion
                        break;

                    case "2":
                        try
                        {
                            TopUpVehicle();
                        }
                        #region catch
                        catch (ArgumentException e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        catch (InvalidOperationException e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        catch (Exception e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        #endregion
                        break;
                    case "3":
                        try
                        {
                            RemoveVehicle();
                        }
                        #region catch
                        catch (ArgumentException e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        catch (InvalidOperationException e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        catch (Exception e)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine(e.Message);
                        }
                        #endregion
                        break;
                    case "b":
                        QuitFlag = false;
                        Console.Clear();
                        continue;
                    default:
                        Console.WriteLine($"\nYou have selected a non-existent option ! Please repeat again.");
                        break;
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write("\nPress the key to select the menu item again... ");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private void PutVehicle()
        {
            Console.Write($"\nInput your vehicle number: ");
            string vehicleId = Console.ReadLine();

            var type = GetVehicleType();

            Console.Write($"Invest the (positive) amount in the parking lot on your vehicle: "); 
            if (!Decimal.TryParse(Console.ReadLine(), out decimal balance))
                throw new ArgumentException("You had to enter a number !!! (decimal type)");

            _parkingServices.AddVehicle(new Vehicle(vehicleId, type, balance));
        }

        private void TopUpVehicle() 
        {
            var vehicleCollection = _parkingServices.GetVehicles();
            if (vehicleCollection.Count != 0)
            {
                byte listLength = WriteListVehicels(vehicleCollection);

                Console.Write($"\nSelect a vehicle to top up: ");

                if (!Int32.TryParse(Console.ReadLine(), out int index))
                    throw new ArgumentException("You had to enter a number !!! (int type)");

                //if index out of range
                while (index <= 0 || index > listLength)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write($"{index} is not included between 1 and {listLength}. Please try again: ");
                    if (!Int32.TryParse(Console.ReadLine(), out int indexRepeat))
                        throw new ArgumentException("You had to enter a number !!! (int type)");
                    index = indexRepeat;
                }
                Console.ForegroundColor = ConsoleColor.White;

                Console.Write($"Top up (positive) amount in the parking lot for your vehicle: ");
                if (!Decimal.TryParse(Console.ReadLine(), out decimal sum))
                    throw new ArgumentException("You had to enter a number !!! (decimal type)");

                _parkingServices.TopUpVehicle(vehicleCollection[index - 1].Id, sum);
            }
            else Console.WriteLine("There are no vehicles to top up.");
        }

        private void RemoveVehicle() 
        {
            var vehicleCollection = _parkingServices.GetVehicles();
            if (vehicleCollection.Count != 0)
            {
                byte listLength = WriteListVehicels(vehicleCollection);

                Console.Write($"\nSelect a vehicle to remove: ");

                if (!Int32.TryParse(Console.ReadLine(), out int index))
                    throw new ArgumentException("You had to enter a number !!! (int type)");

                //if index out of range
                while (index <= 0 || index > listLength)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.Write($"{index} is not included between 1 and {listLength}. Please try again: ");
                    if (!Int32.TryParse(Console.ReadLine(), out int indexRepeat))
                        throw new ArgumentException("You had to enter a number !!! (int type)");
                    index = indexRepeat;
                }
                Console.ForegroundColor = ConsoleColor.White;

                _parkingServices.RemoveVehicle(vehicleCollection[index-1].Id);
            }
            else Console.WriteLine("There are no vehicles to remove.");            
        }

        private VehicleType GetVehicleType()
        {
            byte i = 0;
            Console.ForegroundColor = ConsoleColor.Yellow;
            foreach (var nameType in (VehicleType[])Enum.GetValues(typeof(VehicleType)))
            {
                Console.WriteLine($"\t{++i}. {nameType}");
            }

            Console.ForegroundColor = ConsoleColor.White;
            Console.Write($"Select the type of your transport: ");
                        
            if (!Int32.TryParse(Console.ReadLine(), out int index))
                throw new ArgumentException("You had to enter a number !!! (int type)");

            //if index out of range
            while (index <= 0 || index > i)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write($"{index} is not included between 1 and {i}. Please try again: ");
                if (!Int32.TryParse(Console.ReadLine(), out int indexRepeat))
                    throw new ArgumentException("You had to enter a number !!! (int type)");
                index = indexRepeat;
            }
            Console.ForegroundColor = ConsoleColor.White;

            return (VehicleType)index;
        }

        private byte WriteListVehicels(ReadOnlyCollection<Vehicle> vehicles) 
        {
            Console.WriteLine("\nList of vehicles in the parking lot: \n");
            Console.ForegroundColor = ConsoleColor.Yellow;
            byte i = 0;
            foreach (var item in vehicles)
            {
                Console.WriteLine($"\t{++i}. {item.ToString()}");
            }

            Console.ForegroundColor = ConsoleColor.White;
            return i;
        }
    }
}
