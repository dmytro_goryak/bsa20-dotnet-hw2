﻿using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.View
{
    class SettingsView : IView
    {
        public void Start()
        {
            Console.Clear();

            Console.BackgroundColor = ConsoleColor.Magenta;
            Console.ForegroundColor = ConsoleColor.DarkBlue;

            Console.WriteLine("\nOptions.\n");

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Cyan;

            Console.WriteLine($"Initial balance of Parking: {Settings.Balance}");
            Console.WriteLine($"Parking capacity: {Settings.Capacity}");
            Console.WriteLine($"Payment period: {Settings.PaymentPeriod} sec");
            Console.WriteLine($"The period of writing to the log: {Settings.LogPeriod} sec");
            Console.WriteLine("Tariffs of vehicles");
            foreach (var item in Settings.VehicleRate)            
                Console.WriteLine($"  -{item.Key}: {item.Value}");     
            Console.WriteLine($"Penalty ratio: {Settings.PenaltyRatio}");

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write($"\nPress the key to return in main menu... ");
            Console.ReadKey();
            Console.Clear();

        }
    }
}
