﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
    class FileService : IDisposable
    {
        private FileStream MyStream { get; set; }
        private StreamWriter MyWriter { get; set; }

        private StreamReader MyReader { get; set; }

        private string FilePath;

        public FileService(string filePath)
        {
            this.FilePath = filePath;
        }

        public void CreateNewFile()
        {
            try
            {
                MyStream = File.Create(FilePath);
            }
            finally
            {
                Dispose();
            }
        }

        public void AddTextToFile(string text)
        {
            try
            {
                MyStream = File.Open(FilePath, FileMode.Append);
                MyWriter = new StreamWriter(MyStream);
                MyWriter?.WriteLine(text);
            }
            finally
            {
                Dispose();
            }
        }

        public string ReadFromFile()
        {
            try
            {
                MyStream = File.OpenRead(FilePath);
                MyReader = new StreamReader(MyStream);
                if (MyStream?.Length > 0)
                {
                    string outString = "";
                    while (MyReader?.Peek() >= 0)
                    {
                        outString += MyReader?.ReadLine() + "\n";
                    }

                    return outString;
                }
                else return "The log file is now empty.\n";
            }
            finally
            {
                Dispose();
            }
        }


        // Free unmanaged resources
        public void Dispose()
        {
            MyReader?.Dispose();
            MyWriter?.Dispose();
            MyStream?.Dispose();
        }
    }
}
