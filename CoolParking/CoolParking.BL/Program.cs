﻿using CoolParking.BL.Services;
using CoolParking.BL.View;
using System;

namespace CoolParking.BL
{
    class Program
    {
        
        static void Main(string[] args)
        {
            //initial all service
            TimerService withDraw = new TimerService();
            TimerService logToFile = new TimerService();            
            LogService logService = new LogService(@$"{System.Environment.CurrentDirectory}\Transactions.log");
            ParkingService parkingService = new ParkingService(withDraw,logToFile,logService);

            //initial console view
            Console.ForegroundColor = ConsoleColor.White;
            var view = new MainConsoleView(parkingService);
            view.Start();           
        }        
    }
}
