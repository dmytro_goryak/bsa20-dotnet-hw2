﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }

        private Regex RegexPattern = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            string vehicleId = id.ToUpper();
            if (RegexPattern.IsMatch(vehicleId))  Id = vehicleId;
            else throw new ArgumentException($"\n{vehicleId} it doesn't look like a pattern: 'AA-0000-AA'");

            VehicleType = vehicleType;

            if(balance>0) Balance = balance;
            else throw new ArgumentException($"\n{balance} - You can't put a vehicle in the parking lot with a negative value money");
        }

        public static string GenerateRandomRegistrationPlateNumber() 
        {
            Random random = new Random();
            char[] latters = new char[4];
            int[] numbers = new int[4];

            for (int i = 0; i < latters.Length; i++) 
            {
                latters[i] = (char)random.Next('A', 'Z');
                numbers[i] = random.Next(1, 9);
            }

            return $"{latters[0]}{latters[1]}-{string.Join("", numbers)}-{latters[2]}{latters[3]}";
        }

        public void TopUpBalance(decimal money) 
        {
            if (money > 0) Balance += money;
            else throw new ArgumentException($"\n{money} - You can't pay with a negative value");
        }

        public override string ToString()
        {
            return $"Vehicle ID mumber: {Id} ; Vahicle Type: {VehicleType} ; Balance: {Balance} ";
        }
    }
}
