﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

public static class Settings
{
    public static decimal Balance { get; } = 0;
    public const int Capacity = 10;
    public const int PaymentPeriod = 5;
    public const int LogPeriod = 60;
    public static Dictionary<string, decimal> VehicleRate { get; } = new Dictionary<string, decimal> 
    { 
        { "PassengerCar", 2 },
        { "Truck", 5 },
        { "Bus", (decimal)3.5 },
        { "Motorcycle", 1 } 
    };
    public const decimal PenaltyRatio = (decimal)2.5;
}
